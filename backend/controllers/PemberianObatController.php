<?php

namespace backend\controllers;

use backend\models\PemberianObat;
use backend\models\PemberianObatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PemberianObatController implements the CRUD actions for PemberianObat model.
 */
class PemberianObatController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all PemberianObat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PemberianObatSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PemberianObat model.
     * @param string $no_rawat No Rawat
     * @param string $kode_obat Kode Obat
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($no_rawat, $kode_obat)
    {
        return $this->render('view', [
            'model' => $this->findModel($no_rawat, $kode_obat),
        ]);
    }

    /**
     * Creates a new PemberianObat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PemberianObat();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'no_rawat' => $model->no_rawat, 'kode_obat' => $model->kode_obat]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PemberianObat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $no_rawat No Rawat
     * @param string $kode_obat Kode Obat
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($no_rawat, $kode_obat)
    {
        $model = $this->findModel($no_rawat, $kode_obat);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'no_rawat' => $model->no_rawat, 'kode_obat' => $model->kode_obat]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PemberianObat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $no_rawat No Rawat
     * @param string $kode_obat Kode Obat
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($no_rawat, $kode_obat)
    {
        $this->findModel($no_rawat, $kode_obat)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PemberianObat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $no_rawat No Rawat
     * @param string $kode_obat Kode Obat
     * @return PemberianObat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($no_rawat, $kode_obat)
    {
        if (($model = PemberianObat::findOne(['no_rawat' => $no_rawat, 'kode_obat' => $kode_obat])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
