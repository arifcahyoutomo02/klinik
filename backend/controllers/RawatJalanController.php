<?php

namespace backend\controllers;

use backend\models\RawatJalan;
use backend\models\RawatJalanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RawatJalanController implements the CRUD actions for RawatJalan model.
 */
class RawatJalanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all RawatJalan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RawatJalanSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RawatJalan model.
     * @param string $no_rawat No Rawat
     * @param string $diagnosa Diagnosa
     * @param string $kd_tindakan Kd Tindakan
     * @param string $kd_dokter Kd Dokter
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($no_rawat, $diagnosa, $kd_tindakan, $kd_dokter)
    {
        return $this->render('view', [
            'model' => $this->findModel($no_rawat, $diagnosa, $kd_tindakan, $kd_dokter),
        ]);
    }

    /**
     * Creates a new RawatJalan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RawatJalan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'no_rawat' => $model->no_rawat, 'diagnosa' => $model->diagnosa, 'kd_tindakan' => $model->kd_tindakan, 'kd_dokter' => $model->kd_dokter]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RawatJalan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $no_rawat No Rawat
     * @param string $diagnosa Diagnosa
     * @param string $kd_tindakan Kd Tindakan
     * @param string $kd_dokter Kd Dokter
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($no_rawat, $diagnosa, $kd_tindakan, $kd_dokter)
    {
        $model = $this->findModel($no_rawat, $diagnosa, $kd_tindakan, $kd_dokter);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'no_rawat' => $model->no_rawat, 'diagnosa' => $model->diagnosa, 'kd_tindakan' => $model->kd_tindakan, 'kd_dokter' => $model->kd_dokter]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RawatJalan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $no_rawat No Rawat
     * @param string $diagnosa Diagnosa
     * @param string $kd_tindakan Kd Tindakan
     * @param string $kd_dokter Kd Dokter
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($no_rawat, $diagnosa, $kd_tindakan, $kd_dokter)
    {
        $this->findModel($no_rawat, $diagnosa, $kd_tindakan, $kd_dokter)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RawatJalan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $no_rawat No Rawat
     * @param string $diagnosa Diagnosa
     * @param string $kd_tindakan Kd Tindakan
     * @param string $kd_dokter Kd Dokter
     * @return RawatJalan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($no_rawat, $diagnosa, $kd_tindakan, $kd_dokter)
    {
        if (($model = RawatJalan::findOne(['no_rawat' => $no_rawat, 'diagnosa' => $diagnosa, 'kd_tindakan' => $kd_tindakan, 'kd_dokter' => $kd_dokter])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
