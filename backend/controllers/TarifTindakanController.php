<?php

namespace backend\controllers;

use backend\models\TarifTindakan;
use backend\models\TarifTindakanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TarifTindakanController implements the CRUD actions for TarifTindakan model.
 */
class TarifTindakanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all TarifTindakan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TarifTindakanSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TarifTindakan model.
     * @param string $kd_tindakan Kd Tindakan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($kd_tindakan)
    {
        return $this->render('view', [
            'model' => $this->findModel($kd_tindakan),
        ]);
    }

    /**
     * Creates a new TarifTindakan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TarifTindakan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'kd_tindakan' => $model->kd_tindakan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TarifTindakan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kd_tindakan Kd Tindakan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($kd_tindakan)
    {
        $model = $this->findModel($kd_tindakan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'kd_tindakan' => $model->kd_tindakan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TarifTindakan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kd_tindakan Kd Tindakan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($kd_tindakan)
    {
        $this->findModel($kd_tindakan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TarifTindakan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kd_tindakan Kd Tindakan
     * @return TarifTindakan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kd_tindakan)
    {
        if (($model = TarifTindakan::findOne($kd_tindakan)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
