<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pasien".
 *
 * @property string $no_rkm_medis
 * @property string $nm_pasien
 * @property string $no_ktp
 * @property string $jk
 * @property string $tmp_lahir
 * @property string $tgl_lahir
 * @property string $alamat
 * @property string $gol_darah
 * @property string $pekerjaan
 * @property string $status
 * @property string $agama
 * @property string $tgl_daftar
 * @property string $no_tlp
 * @property string $umur
 * @property string $pendidikan
 * @property string $keluarga
 * @property string $nama_keluarga
 *
 * @property RegPeriksa[] $regPeriksas
 */
class Pasien extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasien';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_rkm_medis', 'nm_pasien', 'no_ktp', 'jk', 'tmp_lahir', 'tgl_lahir', 'alamat', 'gol_darah', 'pekerjaan', 'status', 'agama', 'tgl_daftar', 'no_tlp', 'umur', 'pendidikan', 'keluarga', 'nama_keluarga'], 'required'],
            [['tgl_lahir', 'tgl_daftar'], 'safe'],
            [['gol_darah', 'status', 'pendidikan', 'keluarga'], 'string'],
            [['no_rkm_medis', 'agama'], 'string', 'max' => 10],
            [['nm_pasien'], 'string', 'max' => 40],
            [['no_ktp', 'umur'], 'string', 'max' => 20],
            [['jk'], 'string', 'max' => 12],
            [['tmp_lahir', 'pekerjaan'], 'string', 'max' => 15],
            [['alamat'], 'string', 'max' => 25],
            [['no_tlp'], 'string', 'max' => 13],
            [['nama_keluarga'], 'string', 'max' => 50],
            [['no_rkm_medis'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no_rkm_medis' => Yii::t('app', 'No Rkm Medis'),
            'nm_pasien' => Yii::t('app', 'Nm Pasien'),
            'no_ktp' => Yii::t('app', 'No Ktp'),
            'jk' => Yii::t('app', 'Jk'),
            'tmp_lahir' => Yii::t('app', 'Tmp Lahir'),
            'tgl_lahir' => Yii::t('app', 'Tgl Lahir'),
            'alamat' => Yii::t('app', 'Alamat'),
            'gol_darah' => Yii::t('app', 'Gol Darah'),
            'pekerjaan' => Yii::t('app', 'Pekerjaan'),
            'status' => Yii::t('app', 'Status'),
            'agama' => Yii::t('app', 'Agama'),
            'tgl_daftar' => Yii::t('app', 'Tgl Daftar'),
            'no_tlp' => Yii::t('app', 'No Tlp'),
            'umur' => Yii::t('app', 'Umur'),
            'pendidikan' => Yii::t('app', 'Pendidikan'),
            'keluarga' => Yii::t('app', 'Keluarga'),
            'nama_keluarga' => Yii::t('app', 'Nama Keluarga'),
        ];
    }

    /**
     * Gets query for [[RegPeriksas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegPeriksas()
    {
        return $this->hasMany(RegPeriksa::className(), ['no_rkm_medis' => 'no_rkm_medis']);
    }
}
