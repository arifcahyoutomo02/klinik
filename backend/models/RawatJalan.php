<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "rawat_jalan".
 *
 * @property string $no_rawat
 * @property string $diagnosa
 * @property string $kd_tindakan
 * @property string $kd_dokter
 * @property string $suhu_tubuh
 * @property string $tensi
 * @property string $keterangan
 * @property float $biaya_rawat
 */
class RawatJalan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rawat_jalan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_rawat', 'diagnosa', 'kd_tindakan', 'kd_dokter', 'suhu_tubuh', 'tensi', 'keterangan', 'biaya_rawat'], 'required'],
            [['biaya_rawat'], 'number'],
            [['no_rawat'], 'string', 'max' => 20],
            [['diagnosa'], 'string', 'max' => 30],
            [['kd_tindakan', 'kd_dokter'], 'string', 'max' => 10],
            [['suhu_tubuh'], 'string', 'max' => 5],
            [['tensi'], 'string', 'max' => 7],
            [['keterangan'], 'string', 'max' => 50],
            [['no_rawat', 'diagnosa', 'kd_tindakan', 'kd_dokter'], 'unique', 'targetAttribute' => ['no_rawat', 'diagnosa', 'kd_tindakan', 'kd_dokter']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no_rawat' => Yii::t('app', 'No Rawat'),
            'diagnosa' => Yii::t('app', 'Diagnosa'),
            'kd_tindakan' => Yii::t('app', 'Kd Tindakan'),
            'kd_dokter' => Yii::t('app', 'Kd Dokter'),
            'suhu_tubuh' => Yii::t('app', 'Suhu Tubuh'),
            'tensi' => Yii::t('app', 'Tensi'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'biaya_rawat' => Yii::t('app', 'Biaya Rawat'),
        ];
    }
}
