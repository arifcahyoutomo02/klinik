<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "petugas".
 *
 * @property string $nip
 * @property string $nama
 * @property string $jk
 * @property string $tmp_lahir
 * @property string $tgl_lahir
 * @property string $alamat
 * @property string $password
 *
 * @property RegPeriksa[] $regPeriksas
 */
class Petugas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'petugas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nip', 'nama', 'jk', 'tmp_lahir', 'tgl_lahir', 'alamat', 'password'], 'required'],
            [['jk'], 'string'],
            [['tgl_lahir'], 'safe'],
            [['nip', 'nama', 'tmp_lahir', 'alamat'], 'string', 'max' => 20],
            [['password'], 'string', 'max' => 8],
            [['nip'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nip' => Yii::t('app', 'Nip'),
            'nama' => Yii::t('app', 'Nama'),
            'jk' => Yii::t('app', 'Jk'),
            'tmp_lahir' => Yii::t('app', 'Tmp Lahir'),
            'tgl_lahir' => Yii::t('app', 'Tgl Lahir'),
            'alamat' => Yii::t('app', 'Alamat'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * Gets query for [[RegPeriksas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegPeriksas()
    {
        return $this->hasMany(RegPeriksa::className(), ['nip' => 'nip']);
    }
}
