<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tarif_tindakan".
 *
 * @property string $kd_tindakan
 * @property string $nm_tindakan
 * @property float $tarif
 */
class TarifTindakan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tarif_tindakan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_tindakan', 'nm_tindakan', 'tarif'], 'required'],
            [['tarif'], 'number'],
            [['kd_tindakan'], 'string', 'max' => 10],
            [['nm_tindakan'], 'string', 'max' => 20],
            [['kd_tindakan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_tindakan' => Yii::t('app', 'Kd Tindakan'),
            'nm_tindakan' => Yii::t('app', 'Nm Tindakan'),
            'tarif' => Yii::t('app', 'Tarif'),
        ];
    }
}
