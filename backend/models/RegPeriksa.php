<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "reg_periksa".
 *
 * @property string $no_rawat
 * @property string $tgl_registrasi
 * @property string $jam_reg
 * @property string $kd_dokter
 * @property string $no_rkm_medis
 * @property string $nip
 * @property float $biaya_reg
 *
 * @property Dokter $kdDokter
 * @property Petugas $nip0
 * @property Pasien $noRkmMedis
 */
class RegPeriksa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reg_periksa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_rawat', 'tgl_registrasi', 'jam_reg', 'kd_dokter', 'no_rkm_medis', 'nip', 'biaya_reg'], 'required'],
            [['tgl_registrasi', 'jam_reg'], 'safe'],
            [['biaya_reg'], 'number'],
            [['no_rawat', 'nip'], 'string', 'max' => 20],
            [['kd_dokter', 'no_rkm_medis'], 'string', 'max' => 10],
            [['no_rawat'], 'unique'],
            [['kd_dokter'], 'exist', 'skipOnError' => true, 'targetClass' => Dokter::className(), 'targetAttribute' => ['kd_dokter' => 'kd_dokter']],
            [['nip'], 'exist', 'skipOnError' => true, 'targetClass' => Petugas::className(), 'targetAttribute' => ['nip' => 'nip']],
            [['no_rkm_medis'], 'exist', 'skipOnError' => true, 'targetClass' => Pasien::className(), 'targetAttribute' => ['no_rkm_medis' => 'no_rkm_medis']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no_rawat' => Yii::t('app', 'No Rawat'),
            'tgl_registrasi' => Yii::t('app', 'Tgl Registrasi'),
            'jam_reg' => Yii::t('app', 'Jam Reg'),
            'kd_dokter' => Yii::t('app', 'Kd Dokter'),
            'no_rkm_medis' => Yii::t('app', 'No Rkm Medis'),
            'nip' => Yii::t('app', 'Nip'),
            'biaya_reg' => Yii::t('app', 'Biaya Reg'),
        ];
    }

    /**
     * Gets query for [[KdDokter]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKdDokter()
    {
        return $this->hasOne(Dokter::className(), ['kd_dokter' => 'kd_dokter']);
    }

    /**
     * Gets query for [[Nip0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNip0()
    {
        return $this->hasOne(Petugas::className(), ['nip' => 'nip']);
    }

    /**
     * Gets query for [[NoRkmMedis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoRkmMedis()
    {
        return $this->hasOne(Pasien::className(), ['no_rkm_medis' => 'no_rkm_medis']);
    }
}
