<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pemberian_obat".
 *
 * @property string $no_rawat
 * @property string $diagnosa
 * @property string $kode_obat
 * @property float $biaya_obat
 * @property float $jml
 * @property float $tambahan
 * @property float $total
 */
class PemberianObat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pemberian_obat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_rawat', 'diagnosa', 'kode_obat', 'biaya_obat', 'jml', 'tambahan', 'total'], 'required'],
            [['biaya_obat', 'jml', 'tambahan', 'total'], 'number'],
            [['no_rawat'], 'string', 'max' => 20],
            [['diagnosa'], 'string', 'max' => 30],
            [['kode_obat'], 'string', 'max' => 15],
            [['no_rawat', 'kode_obat'], 'unique', 'targetAttribute' => ['no_rawat', 'kode_obat']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no_rawat' => Yii::t('app', 'No Rawat'),
            'diagnosa' => Yii::t('app', 'Diagnosa'),
            'kode_obat' => Yii::t('app', 'Kode Obat'),
            'biaya_obat' => Yii::t('app', 'Biaya Obat'),
            'jml' => Yii::t('app', 'Jml'),
            'tambahan' => Yii::t('app', 'Tambahan'),
            'total' => Yii::t('app', 'Total'),
        ];
    }
}
