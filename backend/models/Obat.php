<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "obat".
 *
 * @property string $kode_obat
 * @property string $nama_obat
 * @property int $harga
 */
class Obat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'obat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_obat', 'nama_obat', 'harga'], 'required'],
            [['harga'], 'integer'],
            [['kode_obat'], 'string', 'max' => 15],
            [['nama_obat'], 'string', 'max' => 20],
            [['kode_obat'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_obat' => Yii::t('app', 'Kode Obat'),
            'nama_obat' => Yii::t('app', 'Nama Obat'),
            'harga' => Yii::t('app', 'Harga'),
        ];
    }
}
