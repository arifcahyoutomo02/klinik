<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "dokter".
 *
 * @property string $kd_dokter
 * @property string $nama_dokter
 * @property string $jk
 * @property string $tmp_lahir
 * @property string $tgl_lahir
 * @property string $no_ijin_praktik
 * @property string $alamat
 * @property string $password
 */
class Dokter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dokter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_dokter', 'nama_dokter', 'jk', 'tmp_lahir', 'tgl_lahir', 'no_ijin_praktik', 'alamat', 'password'], 'required'],
            [['jk'], 'string'],
            [['tgl_lahir'], 'safe'],
            [['kd_dokter'], 'string', 'max' => 10],
            [['nama_dokter'], 'string', 'max' => 40],
            [['tmp_lahir'], 'string', 'max' => 15],
            [['no_ijin_praktik', 'password'], 'string', 'max' => 20],
            [['alamat'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_dokter' => Yii::t('app', 'Kd Dokter'),
            'nama_dokter' => Yii::t('app', 'Nama Dokter'),
            'jk' => Yii::t('app', 'Jk'),
            'tmp_lahir' => Yii::t('app', 'Tmp Lahir'),
            'tgl_lahir' => Yii::t('app', 'Tgl Lahir'),
            'no_ijin_praktik' => Yii::t('app', 'No Ijin Praktik'),
            'alamat' => Yii::t('app', 'Alamat'),
            'password' => Yii::t('app', 'Password'),
        ];
    }
}
