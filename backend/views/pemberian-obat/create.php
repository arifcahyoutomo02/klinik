<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PemberianObat */

$this->title = Yii::t('app', 'Create Pemberian Obat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pemberian Obats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pemberian-obat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
