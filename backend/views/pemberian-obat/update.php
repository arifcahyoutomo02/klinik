<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PemberianObat */

$this->title = Yii::t('app', 'Update Pemberian Obat: {name}', [
    'name' => $model->no_rawat,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pemberian Obats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->no_rawat, 'url' => ['view', 'no_rawat' => $model->no_rawat, 'kode_obat' => $model->kode_obat]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pemberian-obat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
