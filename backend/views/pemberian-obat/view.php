<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PemberianObat */

$this->title = $model->no_rawat;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pemberian Obats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pemberian-obat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'no_rawat' => $model->no_rawat, 'kode_obat' => $model->kode_obat], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'no_rawat' => $model->no_rawat, 'kode_obat' => $model->kode_obat], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'no_rawat',
            'diagnosa',
            'kode_obat',
            'biaya_obat',
            'jml',
            'tambahan',
            'total',
        ],
    ]) ?>

</div>
