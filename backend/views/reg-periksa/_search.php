<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RegPeriksaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reg-periksa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'no_rawat') ?>

    <?= $form->field($model, 'tgl_registrasi') ?>

    <?= $form->field($model, 'jam_reg') ?>

    <?= $form->field($model, 'kd_dokter') ?>

    <?= $form->field($model, 'no_rkm_medis') ?>

    <?php // echo $form->field($model, 'nip') ?>

    <?php // echo $form->field($model, 'biaya_reg') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
