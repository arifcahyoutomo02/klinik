<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\RegPeriksa */

$this->title = Yii::t('app', 'Update Reg Periksa: {name}', [
    'name' => $model->no_rawat,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reg Periksas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->no_rawat, 'url' => ['view', 'no_rawat' => $model->no_rawat]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="reg-periksa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
