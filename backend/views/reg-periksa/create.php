<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\RegPeriksa */

$this->title = Yii::t('app', 'Create Reg Periksa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reg Periksas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reg-periksa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
