<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Petugas */

$this->title = Yii::t('app', 'Update Petugas: {name}', [
    'name' => $model->nip,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Petugas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nip, 'url' => ['view', 'nip' => $model->nip]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="petugas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
