<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\RawatJalan */

$this->title = Yii::t('app', 'Update Rawat Jalan: {name}', [
    'name' => $model->no_rawat,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rawat Jalans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->no_rawat, 'url' => ['view', 'no_rawat' => $model->no_rawat, 'diagnosa' => $model->diagnosa, 'kd_tindakan' => $model->kd_tindakan, 'kd_dokter' => $model->kd_dokter]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rawat-jalan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
