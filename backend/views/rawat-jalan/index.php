<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RawatJalanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rawat Jalans');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rawat-jalan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Rawat Jalan'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'no_rawat',
            'diagnosa',
            'kd_tindakan',
            'kd_dokter',
            'suhu_tubuh',
            //'tensi',
            //'keterangan',
            //'biaya_rawat',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
