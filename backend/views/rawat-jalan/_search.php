<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RawatJalanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rawat-jalan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'no_rawat') ?>

    <?= $form->field($model, 'diagnosa') ?>

    <?= $form->field($model, 'kd_tindakan') ?>

    <?= $form->field($model, 'kd_dokter') ?>

    <?= $form->field($model, 'suhu_tubuh') ?>

    <?php // echo $form->field($model, 'tensi') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'biaya_rawat') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
