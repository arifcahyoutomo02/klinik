<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\RawatJalan */

$this->title = $model->no_rawat;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rawat Jalans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rawat-jalan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'no_rawat' => $model->no_rawat, 'diagnosa' => $model->diagnosa, 'kd_tindakan' => $model->kd_tindakan, 'kd_dokter' => $model->kd_dokter], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'no_rawat' => $model->no_rawat, 'diagnosa' => $model->diagnosa, 'kd_tindakan' => $model->kd_tindakan, 'kd_dokter' => $model->kd_dokter], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'no_rawat',
            'diagnosa',
            'kd_tindakan',
            'kd_dokter',
            'suhu_tubuh',
            'tensi',
            'keterangan',
            'biaya_rawat',
        ],
    ]) ?>

</div>
