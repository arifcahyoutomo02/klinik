<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TarifTindakan */

$this->title = Yii::t('app', 'Update Tarif Tindakan: {name}', [
    'name' => $model->kd_tindakan,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarif Tindakans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_tindakan, 'url' => ['view', 'kd_tindakan' => $model->kd_tindakan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tarif-tindakan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
