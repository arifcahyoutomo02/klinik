<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TarifTindakan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarif-tindakan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_tindakan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nm_tindakan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tarif')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
