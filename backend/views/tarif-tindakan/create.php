<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TarifTindakan */

$this->title = Yii::t('app', 'Create Tarif Tindakan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarif Tindakans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarif-tindakan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
