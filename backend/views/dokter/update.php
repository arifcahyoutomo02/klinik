<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Dokter */

$this->title = Yii::t('app', 'Update Dokter: {name}', [
    'name' => $model->kd_dokter,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dokters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_dokter, 'url' => ['view', 'kd_dokter' => $model->kd_dokter]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dokter-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
