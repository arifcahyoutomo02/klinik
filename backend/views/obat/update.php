<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Obat */

$this->title = Yii::t('app', 'Update Obat: {name}', [
    'name' => $model->kode_obat,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Obats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_obat, 'url' => ['view', 'kode_obat' => $model->kode_obat]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="obat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
